# Welcome to ToDo!

This is a **ToDo** application which adds tasks with their respective titles and descriptions.


# Tech Stack

 - **axios** for REST API requests. 
 - **Ruby on Rails** for server application and connecting to DB.
 - **Postgresql** for Database.
 - **React JS** for client code.
 - **antd** for components.
 
## More about this application

It uses a service layered architecture where different access controls can hit some service to get the computation done. It uses **SHA1** digest for cookie generation and **Bcrypt** hash for storing passwords which thereby makes a strongly secure hash within very less time involving a cost factor of 12. **Drag sorting** which comes with react antd is used in this application for drag and drop purposes.


## Youtube Link
[Click here to watch the demonstration.](https://youtu.be/hLT7G0cu7rY)