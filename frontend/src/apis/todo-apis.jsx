import axios from 'axios';

const base_url = 'http://localhost:5000';
const SIGNUP = `${base_url}/api/v1/public/signup`;
const SIGNIN = `${base_url}/api/v1/public/signin`;
const LOGOUT = `${base_url}/api/v1/signedin/logout`;

const TASKS = `${base_url}/api/v1/signedin/tasks`;

export const signUp = params => axios.post(SIGNUP, params);
export const signIn = params => axios.post(SIGNIN, params);

export const logOut = params => axios.post(LOGOUT, params);

export const fetchTasks = params => axios.get(TASKS, {params});
export const createTask = params => axios.post(TASKS, params);
export const updateTask = (id, params) => axios.put(`${TASKS}/${id}`, params);
export const reorderTasks = params => axios.post(`${TASKS}/reorder`, params);