import React, { Component } from "react";
import { Button, Form, Input, notification } from "antd";

class Signup extends Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	handleSubmit = (data) => {
		const {signup} = this.props;
		const {name, email, password, confirm_password} = this.state;
		if (password !== confirm_password) {
			notification.open({
				message: 'Passwords mismatch'
			  });
		} else {
			signup(name, email, password);
		}
	}

	setValue = (e) => {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	render() {
		return (
			<Form  className="login-form">
			<Form.Item>
				<Input placeholder="Enter name" className="ant-input" name='name' onChange={this.setValue}/>
			</Form.Item>
			<Form.Item>
				<Input placeholder="Enter username" className="ant-input" name='email' onChange={this.setValue}/>
			</Form.Item>
			<Form.Item>
				<Input.Password placeholder="input password" name='password' className="ant-input"  onChange={this.setValue} />
			</Form.Item>
			<Form.Item>
				<Input.Password placeholder="confirm password" name='confirm_password' className="ant-input"  onChange={this.setValue} />
			</Form.Item>
			<Form.Item>
			  <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.handleSubmit}>
				Signup
			  </Button>
			</Form.Item>
		  </Form>
		);
	}
	
}

export default Signup;
