import React, { Component } from "react";
import { notification, Button, Form, Input, DatePicker, Checkbox } from "antd";
import  DragSortingTable  from '../draggable';
import { fetchTasks, updateTask, createTask, reorderTasks } from '../apis/todo-apis';
import moment from 'moment';

class LoginHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tasks: [],
			is_loading: true,
			is_update: false,
			is_create: false,
			updatable_task: null,
			default_due_date: new Date()
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData = () => {
		const {cookie} = this.props;
		this.setState({is_loading: true});
		const {default_due_date} = this.state;
		fetchTasks({cookie, default_due_date}).then((res) => {
			if (res.data.success) {
				this.setState({tasks: res.data.data, is_loading: false});
			} else {
				notification.open({ message: 'Error while fetching data'});
			}
		});
	}

	create_task = () => {
		this.setState({is_create: true, is_loading: true, is_update: false});
	}

	update_task = (item) => {
		this.setState({is_update: true, is_loading: true, is_create: false, updatable_task: item});
		this.setState({
			title: (item || {}).title,
			description: (item || {}).description,
			is_complete: (item || {}).is_complete,
			due_date: (item || {}).due_date
		});
	}

	handleUpdate = () => {
		const {cookie} = this.props;
		const {updatable_task, title, description, is_complete, due_date} = this.state;
		const body = {
			cookie,
			title,
			description,
			is_complete,
			due_date
		};
		updateTask((updatable_task || {}).id, body).then((res) => {
			if (res.data.success) {
				this.setState({is_update: false, is_loading: false});
				this.setState({ updatable_task: null,  title: '', description: '', is_complete: false, due_date: null});
				this.getData();
			} else {
				notification.open({ message: 'Error while fetching data'});
			}
		});

	}

	handleCreate = () => {
		const {cookie} = this.props;
		const {title, description, due_date} = this.state;
		const body = {
			cookie,
			title,
			description,
			due_date: (due_date || new Date())
		};
		createTask(body).then((res) => {
			if (res.data.success) {
				this.setState({is_create: false, is_loading: false});
				this.setState({ title: '', description: '', due_date: null});
				this.getData();
			} else {
				notification.open({ message: 'Error while fetching data'});
			}
		});

	}

	setValue = (e) => {
		const { name, value } = e.target;
		this.setState({[name]: value});
	}

	handleDateChange = (e, date_string) => {
		this.setState({due_date: date_string});
	}

	handleDueDateChange = (e, date_string) => {
		this.setState({default_due_date: date_string});
	}

	handleCheckboxChange = (e) => {
		this.setState({ is_complete: e.target.checked });
	}

	reorder = (data) => {
		this.setState({is_loading: true});
		const task_order = data.map((item) => item.id);
		const {default_due_date=new Date()} = this.state;
		const {cookie} = this.props;
		const body = {
			cookie,
			task_order,
			due_date: default_due_date || new Date()
		}
		reorderTasks(body).then((res) => {
			if (res.data.success) {
				this.setState({ is_loading: false, due_date: null});
				this.getData();
			} else {
				notification.open({ message: 'Error while fetching data'});
			}
		});
	}

	render() {
		const columns = [
			{
			  title: 'Title',
			  dataIndex: 'title',
			  key: 'title',
			},
			{
			  title: 'Description',
			  dataIndex: 'description',
			  key: 'description',
			},
			{
			  title: 'Is Completed',
			  dataIndex: 'is_completed',
			  key: 'is_completed'
			},
			{
				title: 'Action',
				dataIndex: 'is_completed',
				key: 'action',
				render: (text, record) => (
				  <span>
					<Button type="primary" htmlType="submit" className="login-form-button" onClick={() => this.update_task(record)}>
							Update
						  </Button>
				  </span>
				),
			  }
		];
		const {tasks, is_loading, is_update, due_date, description, title, is_complete, is_create, default_due_date} = this.state;
		const {logout, cookie, name} = this.props;

		return (
			<>
			<div>
				WELCOME {name}  
			</div>

			
			
			{!is_loading && 
			<>
				<DatePicker name='default_due_date' value={moment(default_due_date)} onChange={this.handleDueDateChange} />
				<Button type="primary" htmlType="submit" className="login-form-button" onClick={() => this.getData()}>
					Fetch
				</Button>
				<br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
				<Button type="primary" htmlType="submit" className="login-form-button" onClick={() => this.create_task()}>
					Create
				</Button>
				
				  <DragSortingTable data={tasks} columns={columns} reorder={this.reorder} />


				<Button type="primary" htmlType="submit" className="login-form-button" onClick={() => logout(cookie)}>
					LOGOUT
				</Button>
			</>}
			{
				is_update &&
				<Form  className="login-form">
				<Form.Item>
					<Input name='title' value={title} onChange={this.setValue}/>
				</Form.Item>
				<Form.Item>
					<Input name='description' value={description} onChange={this.setValue} />
				</Form.Item>
				<Form.Item>
					<DatePicker name='due_date' value={moment(due_date)} onChange={this.handleDateChange} />
				</Form.Item>
				<Form.Item>
					<Checkbox name='is_complete' checked={is_complete} onChange={this.handleCheckboxChange} />
				</Form.Item>
				<Form.Item>
			  	<Button type="primary" htmlType="submit" className="login-form-button" onClick={this.handleUpdate}>
					Update
			  	</Button>
			</Form.Item>
		  </Form>
			}
			{
				is_create &&
				<Form  className="login-form">
				<Form.Item>
					<Input name='title' placeholder='input title' value={title} onChange={this.setValue}/>
				</Form.Item>
				<Form.Item>
					<Input name='description' placeholder='input description' value={description} onChange={this.setValue} />
				</Form.Item>
				<Form.Item>
					<DatePicker name='due_date' value={moment(due_date)} onChange={this.handleDateChange} />
				</Form.Item>
				<Form.Item>
			  	<Button type="primary" htmlType="submit" className="login-form-button" onClick={this.handleCreate}>
					Create
			  	</Button>
			</Form.Item>
		  </Form>
			}
		</>
		);
	}
}

export default LoginHome;
