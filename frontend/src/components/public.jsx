import React, { Component } from 'react';
import { Row, Col, Button, notification } from 'antd';
import Login from './login';
import Signup from './signup';
import LoginHome from './login_home';
import { signUp, signIn, logOut } from '../apis/todo-apis';

class Public extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: '',
			zoom: 11,
			showLogin: false,
			showHome: true,
			logged_in: false,
			cookie: '',
		};
	}

	allFalse = () => {
		this.setState({ showLogin: false, showHome: false, showSignup: false });
	}

	handleLogin = () => {
		this.allFalse();
		this.setState({ showLogin: true });
	} 

	handleSignup = () => {
		this.allFalse();
		this.setState({ showSignup: true });
	}

	home = () => {
		this.allFalse();
		this.setState({ showHome: true });
	}

	login = (email, password) => {
		signIn({ email, password }).then((res) => {
			if (res.data.success) {
				this.allFalse();
				this.setState({cookie: res.data.data.token, logged_in: true, name: res.data.data.name});
			} else {
				notification.open({ message: 'Incorrect credentials'});
			}
		});
		
	}

	signup = (name, email, password) => {
		signUp({name, email, password}).then((res) => {
			if (res.data.success) {
				this.home();
			} else {
				notification.open({ message: 'Error while signup'});
			}
		});
	}

	logout = (cookie) => {
		logOut({cookie}).then((res) => {
			if (res.data.success) {
				this.setState({cookie: '', logged_in: false});
				this.home();
			} else {
				notification.open({ message: 'Error while logout'});
			}
		});		
	}

	render() {
		const { showLogin, showHome, showSignup, logged_in, cookie, name } = this.state; 
		return (
			<>
			<Row type="flex" align="middle">
				{
					showHome && 
					<>
					<Col md={10}>
					<Button onClick= {this.handleLogin}>
						LOGIN
					</Button>
					</Col>
    				<Col md={10}>
					<Button onClick={this.handleSignup}>
						SIGNUP
					</Button>
					</Col>
					</>
				}
			</Row>
			<Row>
				{
					showLogin && 
					<Login
						login={this.login}
					/>
				}
			</Row>
			<Row>
				{
					showSignup && 
					<Signup
						signup={this.signup}
					/>
				}
			</Row>
			<Row>
				{
					logged_in &&
					<LoginHome
						logout={this.logout}
						cookie={cookie}
						name={name}
					/>
				}			
			</Row>		
			</>
			
		);
	  }
}

export default Public;