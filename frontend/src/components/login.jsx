import React, { Component } from "react";
import { Button, Form, Input } from "antd";

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	handleSubmit = (data) => {
		const {login} = this.props;
		const {email, password} = this.state;
		login(email, password);
	}

	setValue = (e) => {
		const { name, value } = e.target;
		this.setState({ [name]: value });
		console.log(this.state);
	}

	render() {
		return (
			<Form  className="login-form">
			<Form.Item>
				<Input placeholder="Enter username" className="ant-input" name='email' onChange={this.setValue}/>
			</Form.Item>
			<Form.Item>
				<Input.Password placeholder="input password" name='password' className="ant-input"  onChange={this.setValue} />
			</Form.Item>
			<Form.Item>
			  <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.handleSubmit}>
				Log in
			  </Button>
			</Form.Item>
		  </Form>
		);
	}
	
}

export default Login;
