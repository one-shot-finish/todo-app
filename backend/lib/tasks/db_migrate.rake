namespace :db do
  task :migrate do
    ActiveRecord::Base.establish_connection
    ActiveRecord::Migrator.migrate("db/migrate")

  end

end
