class TaskService
  def self.fetch_all_tasks(user, params)
    today = Date.today
    due_date = params[:default_due_date].present? ? Date.parse(params[:default_due_date]) : today
    tasks = user.tasks.where(is_complete: false, due_date: due_date).order('updated_at desc')
    tasks_json = tasks.as_json(Task.as_json_query_list)
    existing_task_order = user.task_orders.where(due_date: due_date).first
    tasks_json = tasks_json.sort_by { |t| existing_task_order.task_order.index(t['id']) } if existing_task_order.present?

    return {
        data: tasks_json,
        success: true
    }
  end

  def self.create_task(user, params)
    title = params[:title]
    raise 'Title is not present' if title.blank?
    due_date = Date.parse(params[:due_date]) if params[:due_date].present?
    description = params[:description]
    is_complete = false
    task = user.tasks.create!({ title: title, due_date: due_date, description: description, is_complete: is_complete })
    task_order_instance = user.task_orders.where(due_date: due_date).first

    if task_order_instance.present?
      task_order = task_order_instance.task_order
      task_order << task.id
      task_order_instance.update!(task_order: task_order)
    end

    return {
        data: task.as_json(Task.as_json_query),
        success: true
    }
  end

  def self.update_task(user, task, params)
    title = params[:title]
    due_date = Date.parse(params[:due_date]) if params[:due_date].present?
    description = params[:description]
    is_complete = params[:is_complete]

    if due_date != task.due_date
      earlier_due_date = task.due_date
      task_order_instance = user.task_orders.where(due_date: earlier_due_date).first
      if task_order_instance.present?
        task_order = task_order_instance.task_order
        task_order.delete(task.id)
        task_order_instance.update!(task_order: task_order)
      end

      task_order_instance = user.task_orders.where(due_date: due_date).first
      if task_order_instance.present?
        task_order = task_order_instance.task_order if task_order_instance.present?
        task_order << task.id
        task_order_instance.update!(task_order: task_order)
      end

    end

    update_params = {
        title: title,
        due_date: due_date,
        description: description,
        is_complete: is_complete
    }
    update_params = update_params.select{|k, v| v.present?}

    task.update!(update_params)
    return {
        success: true,
        task: task.as_json(Task.as_json_query)
    }
  end

  def self.reorder_tasks(user, params)
    due_date = params[:due_date]
    task_order = params[:task_order]
    raise 'Due date or task order is missing' if due_date.blank? or task_order.blank?
    task_ids = user.tasks.where(due_date: due_date, is_complete: false).pluck(:id)
    raise 'Incorrect order' unless task_order.sort == task_ids.sort
    existing_task_order = user.task_orders.where(due_date: due_date).first
    if existing_task_order.present?
      existing_task_order.update!(task_order: task_order)
    else
      user.task_orders.create!(due_date: due_date, task_order: task_order)
    end
    return {
        success: true
    }

  end

end
