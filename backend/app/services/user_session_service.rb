class UserSessionService
  def self.signin(params)
    email = params[:email]
    password = params[:password]
    raise 'Missing values, plz check' if email.blank? or password.blank?
    user = User.where(email: email).first
    raise 'User does not exists' if user.blank?

    is_authenticated = user.authenticate(password)
    raise 'Invalid password' unless is_authenticated
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    user.user_sessions.create!({ token: token, is_active: true})
    return {
      data: {token: token, name: user.name},
      success: true
    }
  end

  def self.logout(user, params)
    token = params[:cookie]
    user_session = user.user_sessions.where(token: token, is_active: true).first
    raise 'Session does not exists' if user_session.blank?
    user_session.update!(is_active: false)
    return { success: true }
  end

end
