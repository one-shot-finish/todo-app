class UserService
  def self.fetch_user(cookie)
    user_session = UserSession.where(token: cookie, is_active: true).first
    user = user_session.present? ? user_session.user : nil
    return user
  end

  def self.create_user(params)
    name = params[:name]
    email = params[:email].downcase
    password = params[:password]

    raise 'Missing fields in signin, plz fill name, email , password' if name.blank? or email.blank? or password.blank?

    existing_user = User.where(email: email).first
    raise 'Email already exists' if existing_user.present?

    user = User.create!({ name: name, email: email, password: password })
    return {
        data: user.as_json(User.as_json_query),
        success: true
    }
  end

end
