class Task < ApplicationRecord

  belongs_to :user, class_name: 'User'

  def self.as_json_query
    return {
        only: [:id, :is_complete, :title, :description, :due_date],
        include: {
            user: User.as_json_query
        }
    }

  end

  def self.as_json_query_list
    return {
        only: [:id, :is_complete, :title, :description, :due_date]
    }
  end

end
