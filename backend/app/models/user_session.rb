class UserSession < ApplicationRecord
  belongs_to :user, class_name: 'User'

  def self.as_json_query
    return {
        only: [:user_id, :is_active],
        include: {
            user: User.as_json_query
        }
    }

  end

end
