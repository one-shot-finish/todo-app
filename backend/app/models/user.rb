class User < ApplicationRecord
  include ActiveModel::SecurePassword
  has_secure_password

  has_many  :tasks,         class_name: 'Task'
  has_many  :user_sessions, class_name: 'UserSession'
  has_many  :task_orders,   class_name: 'TaskOrder'

  def self.as_json_query
    return {
        only: [:id, :name, :email]
    }

  end

end
