class TaskOrder < ApplicationRecord

  belongs_to :user, class_name: 'User'

  def self.as_json_query
    return {
        only: [:due_date, :task_order],
        include: {
            user: User.as_json_query
        }
    }

  end

end
