class SignedinController < ApplicationController
  before_action :set_current_user

  def set_current_user
    @current_user ||= UserService.fetch_user(params[:cookie])
    raise 'Unauthenticated User' if @current_user.blank?
  end

end
