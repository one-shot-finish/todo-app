class Api::V1::Signedin::TodoController < SignedinController

  before_action :set_task, only: [:update_task]

  def fetch_tasks
    response = TaskService.fetch_all_tasks(@current_user, task_params)
    render json: response
  end

  def create_task
    response = TaskService.create_task(@current_user, create_task_params)
    render json: response
  end

  def update_task
    response = TaskService.update_task(@current_user, @task, update_task_params)
    render json: response
  end

  def reorder
    response = TaskService.reorder_tasks(@current_user, reorder_task_params)
    render json: response
  end

  def logout
    response = UserSessionService.logout(@current_user, logout_params)
    render json: response
  end

  private

  def task_params
    params.permit(:default_due_date)
  end

  def create_task_params
    params.permit(:title, :due_date, :description)
  end

  def update_task_params
    params.permit(:title, :description, :due_date, :is_complete)
  end

  def reorder_task_params
    params.permit(:due_date,  task_order: [])
  end

  def logout_params
    params.permit(:cookie)
  end

  def set_task
    @task = @current_user.tasks.where(id: params[:id]).first
    raise 'task does not exists' if @task.blank?
  end

end
