class Api::V1::Public::TodoController < ApplicationController
  def signup
    response = UserService.create_user(create_user_params)
    render json: response
  end

  def signin
    response = UserSessionService.signin(signin_params)
    render json: response
  end

  private

  def create_user_params
    params.permit(:name, :email, :password)
  end

  def signin_params
    params.permit(:email, :password)
  end

end

