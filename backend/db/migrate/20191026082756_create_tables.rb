class CreateTables < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'
    create_table :users, id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|

      t.string :name,             null: false
      t.string :email,            null: false,  index: true
      t.string :password_digest,  null: false

      t.timestamps

    end

    create_table :tasks, id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
      t.uuid      :user_id,       null: false,  index: true
      t.boolean   :is_complete,   null: false,  index: true
      t.string    :title,         null: false
      t.string    :description
      t.date      :due_date,                    index: true

      t.timestamps

    end
    add_foreign_key :tasks, :users, column: :user_id

    create_table :user_sessions, id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
      t.uuid    :user_id,         null: false,  index: true
      t.string  :token,           null: false,  index: true
      t.boolean :is_active,       null: false

      t.timestamps

    end
    add_foreign_key :user_sessions, :users, column: :user_id

    create_table :task_orders, id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
      t.uuid      :user_id,       null: false,  index: true
      t.date      :due_date,      null: false,  index: true
      t.uuid      :task_order,    null: false,                array: true

      t.timestamps

    end
    add_foreign_key :task_orders, :users, column: :user_id

  end

end
