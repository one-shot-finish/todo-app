Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      namespace :public do
        post  'signup'    =>  'todo#signup'
        post  'signin'        =>  'todo#signin'
      end

      namespace :signedin do
        get   'tasks'         =>  'todo#fetch_tasks'
        post  'tasks'         =>  'todo#create_task'

        put   'tasks/:id'     =>  'todo#update_task'
        post  'tasks/reorder' =>  'todo#reorder'
        post  'logout'        =>  'todo#logout'
      end

    end

  end

end
